class Drop < Formula
    desc "kubectl drop"
    homepage "https://gitlab.com/danacr/kubectl-drop"
    url "https://gitlab.com/danacr/kubectl-drop/raw/master/kubectl-drop.tar.gz"
    sha256 "1856f046d4c8bd5129c48d6eabcac9dc68788a711e934ed8dec84ec9360465ea"
  
    bottle :unneeded
  
    def install
      bin.install "kubectl-drop"
    end
  
    test do
      system "#{bin}/kubectl-drop", "version"
    end
  end
